var mongoose = require('mongoose');
var config = require('../config/config');
var db = mongoose.connect("mongodb://"+config.MongoDB.USER+":"+config.MongoDB.PASSWORD+"@" 
	+ config.MongoDB.HOST + ":" + config.MongoDB.PORT + "/" + config.MongoDB.NAME);

module.exports = db;