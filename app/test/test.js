'use strict';

let assert = require('assert'),
    request = require('co-supertest'),
    app     = require('./app');


require('co-mocha');

let server = app.listen();


function testHelper(swigFile, contains, doLog, data) {
  data=data || {};
  return request(server)
    .post('/')
    .set('swigFile',swigFile)
    .set('doLog', doLog)
    .send(data)
    .expect(200)
    .expect(contains)
    .end();

}

describe('Swing', function () {

  it('渲染一个传递过来的变量', function *(){
    let name;
    name = 'James';
    yield testHelper('var', new RegExp(name), true, {name: name})
  })

  it('自动转义危险的字符',function *(){
       let name = 'James';
      yield testHelper('var', new RegExp(name), true, {hello: name})
  })

})
