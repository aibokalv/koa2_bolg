'use strict';


let koa = require('koa'),
    views = require('co-views'),
    colors = require('colors'),
    parse = require('co-body');

let app =new koa();

let render = views(__dirname + '/swig/',{default: 'swig'});

app.use(function *controller(){


  let fileName, doLog, data, body;
  fileName = this.get('swigFile');
  doLog = this.get('doLog');
  data = yield parse(this);
  body = yield render(fileName, data)
  if(doLog ){
      console.log(body.white.bgCyan);
  }else{

  }
  // console.log(this.get('doLog'));
  this.body =body ;
})

module.exports =app
