/**!
 * Demos - route/api_route.js
 *
 * 子路由，开放API
 *
 * Authors:
 *  Berwin <liubowen.niubi@gmail.com>
 */

'use strict';

import Router from 'koa-router';
import userCtrl from '../controllers/api/user.js';
import auth from '../middlewares/auth.js';

import config from '../../config/config'

let router=Router()

// 认证合法性
// router.post('*', async (ctx)=>{
// 	auth()
// });


/*
 * user 相关路由
 */
router
  .get('/user', async (ctx)=>{
  	// console.log("获取到get")
  	 await ctx.render('index', { title: '这里是通过api借口获取的',content:'api返回内容' })
  })

module.exports = router;
