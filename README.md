# koa2_bolg


# 规则表达式

```

{
  "rules": {
    "info": {
      ".read": "auth != null && root.child('admins').hasChild(auth.uid)",
      ".write": "auth != null && root.child('admins').hasChild(auth.uid)"
    },
    "users":{
      ".read": false,
      ".write": false
    },
     "admins": {

      ".read": "auth.uid==='9ce845659c5285ca137db348cd93'",
      ".write": "auth.uid==='9ce845659c5285ca137db348cd93'"
    }

  }
}
```
