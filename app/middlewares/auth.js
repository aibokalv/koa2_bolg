import helper from '../helper/index'
import config from '../../config/config'

const auth=async ()=>{
	var passMd5 = helper.getMd5(this.request.body.id + config.md5_suffix);

  if (this.cookies.get(passMd5)) {
    await next();
  } else {
    this.status = 403;
    this.body = 'Unable to pass authentication';
  }
}
export default auth