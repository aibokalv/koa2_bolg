import crypto from 'crypto'
import request from 'request'
import config from '../../config/config'

/**
 * 获取MD5
 * @aibokalv@163.com
 * @DateTime         2016-07-12T14:25:08+0800
 * @param            {string}                 str 任意字符串
 * @return           {String}                     MD5后的字符串
 */

const getMd5=async (str)=>{
	if(!str&&arguments.length===0) return '';

	let md5=crypto.createHash('md5')
	return md5.update(str,'utf-8').digest('hex')
}
export default getMd5 