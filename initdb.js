var mongoose = require('mongoose');
var config = require('./config/config');
var db = mongoose.connect("mongodb://"+config.MongoDB.USER+":"+config.MongoDB.PASSWORD+"@" 
	+ config.MongoDB.HOST + ":" + config.MongoDB.PORT + "/" + config.MongoDB.NAME);

var TestSchema=new mongoose.Schema({
	name:{type:String},
	age:{type:Number,default:0},
	email:{type:String},
	createTime:{type:Date,default:Date.now},
	updataTime:{type:Date}
});

var TestModle=db.model('user',TestSchema);
var TestEntity=new TestModle({
	name:'helloworld',
	age:28,
	email:'aibokalv@163.com',
	updataTime:''
});

TestEntity.save(function(error,doc){
	if(error){
		console.log("error:"+error);
	}else{
		console.log("成功创建user表，并插入一条数据"+doc);
		return true
	}
});

