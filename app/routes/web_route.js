/**
 * web端路由
 */

import Router from 'koa-router';

import 'whatwg-fetch';
import 'wilddog'
let router = Router()

/*
 * 前台 相关路由
 */
router
    .get('/', async(ctx) => {
        console.log(ctx)
        await ctx.render('index', {
            title: 'Koa2-Easy',
            content: 'this is index page'
        })
    })
    .get('/login/:id',async(ctx) => {
      var content = ctx.params.id
      console.log(ctx.params)
      await ctx.render('index', {
          title: 'Koa2-Easy',
          content:content
      })
    })

module.exports = router;
